# What is this repository for? 

* to simulate functions of CM7 so that we have a smaller code base for easy upgrade the spring framework.

# Set up
(linux only, requires docker)

## Code base setup
- run git clone then init the submodules.

## System deployment
* Download tomcat and extract it
* Summary of set up
  - (one-time-setup) make an alias for mvn to run java 11 if your system use other jdk by default: `alias mvn11='JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/ mvn'`
  - (one-time-setup) add tomcat bin to your system path: `path+=("/path/to/apache-tomcat-<version>/bin")`
  - (run-if-not) at root project, start mongodb and redis: `docker-compose up -d`
  - build: `mvn11 clean install`
  - deploy: `rm -rf /path/to/apache-tomcat-<version>/webapps/console*; cp console/target/console.war /path/to/apache-tomcat-<version>/webapps; cp scheduler-spring-boot/target/scheduler.war /path/to/apache-tomcat-<version>/webapps; catalina.sh run`
  - press Ctrl+C to stop tomcat.
